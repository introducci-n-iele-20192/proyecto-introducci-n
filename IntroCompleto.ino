// Librerias necesarias
#include "FirebaseESP8266.h"
#include <ESP8266WiFi.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

// Definición de constantes para la base de datos - SE DEBE CAMBIAR ESTO PARA CADA UNO
#define FIREBASE_HOST "intro-database-test.firebaseio.com"
#define FIREBASE_AUTH "QxxkvyYBtWzGA8RDX7iqMwy7zYJ7bXunkF6wmN1x"
#define WIFI_SSID "FAMILIA_GARRIDO"
#define WIFI_PASSWORD "06096320"

// Creación del objeto de la base de datos
FirebaseData firebaseData;

// Pin de lectura del sensor de CO2 - MQ35
int outputpin = A0;

// Creación del objeto de GPS y pines donde se conectará el GPS
TinyGPSPlus tinygps;
int RXPin = 4; // Pin digital D2 en el Node MCU
int TXPin = 3; // Pin digital D1 en el Node MCU
int ledPin = 12; // Pin digital D6 en el Node MCU
// Creación del puerto serial del GPS
SoftwareSerial gps(RXPin, TXPin);

// Variables necesarias para la base de datos
unsigned long sendDataPrevMillis = 0;
String path = "/DataStream/Ejemplo";
uint16_t count = 0;

void setup() {

  // Inicialización del Serial
  Serial.begin(115200);
  // Inicialización del puerto GPS
  gps.begin(9600);
  // Conexión al HotSpot
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Intentando conexión con Dispositivo WiFi: " + String(WIFI_SSID));
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(300);
  }
  Serial.println();
  Serial.print("Conectado con IP: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  // Conexión a la base de datos
  Serial.print("Intentando conexión con base de datos: " + String(FIREBASE_HOST));
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);
  if (!Firebase.beginStream(firebaseData, path))
  {
    Serial.println("------------------------------------");
    Serial.println("No se pudo iniciar el streaming de datos...");
    Serial.println("ERROR: " + firebaseData.errorReason());
    Serial.println("------------------------------------");
    Serial.println();
    delay(1000);
  }
  else
  {
    Serial.println("Se ha iniciado el streaming de datos... ");
  }
}


void loop() {

  //////////////////// LECTURA DIGITAL DEL SENSOR DE GPS A TRAVÉS DE LA LIBRERIA PROPUESTA ///////////////
  // Mientras el sensor envía información
  while (gps.available() > 0) {
    // Antes de recibir información valida por parte del sensor, se asegura que el led este apagado
    digitalWrite(ledPin, LOW);
    tinygps.encode(gps.read());
    // Verificación de la información valida enviada por el GPS
    if (tinygps.location.isUpdated()) {
      // Se guardan en variables la latitud y longitud leída
      Serial.print("Latitude= ");
      float latitud = tinygps.location.lat();
      Serial.print(tinygps.location.lat(), 6);
      Serial.print(" Longitude= ");
      float longitud = tinygps.location.lng();
      Serial.println(tinygps.location.lng(), 6);
      // Cada vez que lleguen datos validos se encenderá un LED
      digitalWrite(ledPin, HIGH);
    }
  }

  /////////////// LECTURA ANALOGA DE LOS DATOS DE CO2 DESDE EL SENSOR MQ135 ///////////////////////
  int co2value = analogRead(outputpin);
  Serial.print("Valor medido de CO2: ");
  Serial.println(co2value);

  /////////////// ENVIO DE DATOS A LA BASE DE DATOS //////////////////////////////////////////////

  // Envio de datos de LONGITUD
  Serial.println("------------------------------------");
  if (Firebase.setDouble(firebaseData, path + "/Longitud/Dato" + String(count),  longitud))
  {
    Serial.println("Dato de Nivel de Longitud enviado con éxito");
    Serial.print("Valor enviado: ");
    // Verificación del Mensaje enviado
    if (firebaseData.dataType() == "int")
      Serial.println(firebaseData.
                     intData());
    else if (firebaseData.dataType() == "float")
      Serial.println(firebaseData.floatData(), 5);
    else if (firebaseData.dataType() == "double")
      printf("%.9lf\n", firebaseData.doubleData());
    else if (firebaseData.dataType() == "boolean")
      Serial.println(firebaseData.boolData() == 1 ? "true" : "false");
    else if (firebaseData.dataType() == "string")
      Serial.println(firebaseData.stringData());
    else if (firebaseData.dataType() == "json")
      Serial.println(firebaseData.jsonData());
    Serial.println("------------------------------------");
    Serial.println();
  }
  else
  {
    Serial.println("Fallo envio de datos");
    Serial.println("ERROR: " + firebaseData.errorReason());
    Serial.println("------------------------------------");
    Serial.println();
  }

  // Envio de datos de LATITUD
  Serial.println("------------------------------------");
  if (Firebase.setDouble(firebaseData, path + "/Latitud/Dato" + String(count),  latitud))
  {
    Serial.println("Dato de Nivel de Latitud enviado con éxito");
    Serial.print("Valor enviado: ");
    // Verificación del Mensaje enviado
    if (firebaseData.dataType() == "int")
      Serial.println(firebaseData.
                     intData());
    else if (firebaseData.dataType() == "float")
      Serial.println(firebaseData.floatData(), 5);
    else if (firebaseData.dataType() == "double")
      printf("%.9lf\n", firebaseData.doubleData());
    else if (firebaseData.dataType() == "boolean")
      Serial.println(firebaseData.boolData() == 1 ? "true" : "false");
    else if (firebaseData.dataType() == "string")
      Serial.println(firebaseData.stringData());
    else if (firebaseData.dataType() == "json")
      Serial.println(firebaseData.jsonData());
    Serial.println("------------------------------------");
    Serial.println();
  }
  else
  {
    Serial.println("Fallo envio de datos");
    Serial.println("ERROR: " + firebaseData.errorReason());
    Serial.println("------------------------------------");
    Serial.println();
  }


  // Envio de datos de CO2
  Serial.println("------------------------------------");
  if (Firebase.setDouble(firebaseData, path + "/CO2/Dato" + String(count), co2value ))
  {
    Serial.println("Dato de Nivel de CO2 enviado con éxito");
    Serial.print("Valor enviado: ");
    // Verificación del Mensaje enviado
    if (firebaseData.dataType() == "int")
      Serial.println(firebaseData.
                     intData());
    else if (firebaseData.dataType() == "float")
      Serial.println(firebaseData.floatData(), 5);
    else if (firebaseData.dataType() == "double")
      printf("%.9lf\n", firebaseData.doubleData());
    else if (firebaseData.dataType() == "boolean")
      Serial.println(firebaseData.boolData() == 1 ? "true" : "false");
    else if (firebaseData.dataType() == "string")
      Serial.println(firebaseData.stringData());
    else if (firebaseData.dataType() == "json")
      Serial.println(firebaseData.jsonData());
    Serial.println("------------------------------------");
    Serial.println();
  }
  else
  {
    Serial.println("Fallo envio de datos");
    Serial.println("ERROR: " + firebaseData.errorReason());
    Serial.println("------------------------------------");
    Serial.println();
  }


  // Otras verificaciones
  if (firebaseData.streamTimeout())
  {
    Serial.println("Stream timeout, resume streaming...");
    Serial.println();
  }

  if (firebaseData.streamAvailable())
  {
    Serial.println("------------------------------------");
    Serial.println("Stream Data available...");
    Serial.println("STREAM PATH: " + firebaseData.streamPath());
    Serial.println("EVENT PATH: " + firebaseData.dataPath());
    Serial.println("DATA TYPE: " + firebaseData.dataType());
    Serial.println("EVENT TYPE: " + firebaseData.eventType());
    Serial.print("VALUE: ");
    if (firebaseData.dataType() == "int")
      Serial.println(firebaseData.intData());
    else if (firebaseData.dataType() == "float")
      Serial.println(firebaseData.floatData());
    else if (firebaseData.dataType() == "boolean")
      Serial.println(firebaseData.boolData() == 1 ? "true" : "false");
    else if (firebaseData.dataType() == "string")
      Serial.println(firebaseData.stringData());
    else if (firebaseData.dataType() == "json")
      Serial.println(firebaseData.jsonData());
    Serial.println("------------------------------------");
    Serial.println();
  }
}
